import { Component } from 'react'
import Taro from '@tarojs/taro';
import { View, Text, Button } from '@tarojs/components'
import './index.scss'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <Button onClick={()=>{
          console.log('测试地图选择。。。。')
          Taro.chooseLocation().then((res)=>{
            console.log('选择的地图值是：',res)
          }).catch((error)=>{
            console.log('选择地图的错误值是：',error);
          });
        }}>测试</Button>
      </View>
    )
  }
}
